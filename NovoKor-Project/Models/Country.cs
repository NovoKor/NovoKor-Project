﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovoKorProject.Models
{
    public class Country
    {
    public int ID { get; set; }
    public string CountryAbb { get; set; }
    public string CountryName { get; set; }
    public float CountryEmp { get; set; }
    public float CountryTurnOver { get; set; }
    public float CountryDensity { get; set; }
    public float CountryTech { get; set; }
    public float CountryProduct { get; set; }
    public float CountryMark { get; set; }
    public float CountryInnProcess { get; set; }
    public float CountryTurnOut { get; set; }
    public float CountryINN { get; set; }

     }
}
