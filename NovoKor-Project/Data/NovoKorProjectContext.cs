﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NovoKorProject.Models
{
    public class NovoKorProjectContext : DbContext
    {
        public NovoKorProjectContext (DbContextOptions<NovoKorProjectContext> options)
            : base(options)
        {
        }

        public DbSet<NovoKorProject.Models.Country> Country { get; set; }
    }
}
